# LEDStripeServer for Raspberry Pi Boards
### Usage
Run the Server.py script on your Raspberry Pi (command: `python Server.py`) and control
the connected LED Stripe with the [Android app](https://bitbucket.org/BavarianCoders/ledstripeclient)

### master branch
It's a simple server written in Python that can receive RGB color values  
and send them to the **pigpiod daemon** to control the color of a LED stripe.

### rpi.gpio branch
The python program in this branch offers the same functionality as the program  
in the master branch but uses the **RPI.GPIO** python library to control the LEDs.  
This variant of the program runs on more Raspberry Pi OSes. For example on LibreElec  
if raspberry pi tools addon is installed.

### Important to know
To get this working you definitely need a **LED controller** which is connected 
to the Raspberry Pi's GPIO pins. Sadly no one offers a complete setup for sale 
but there is a awesome [guide](http://dordnung.de/raspberrypi-ledstrip/) from David 
Ordnung where he explains in detail how to build a LED controller on a breadboard.